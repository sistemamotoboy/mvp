var fs 			= require('fs'),
		path 		= require('path'),
		hbs			= require('koa-hbs'),
		config	= require('./config.json');

module.exports = function (app) {

	require(config.routes)(app);
	require(config.models)(app);

}
