var mongoose 	= require('mongoose'),
		config 		= require('../config.json');

module.exports = function (app) {

	mongoose.connect(
		'mongodb://' + config.db.url + '/' + config.db.name,
		function (err, res) {
			if (!err) {
				console.log('database connected')
			} else {
				console.log(err)
			}
	});
}
